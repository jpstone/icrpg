alter table loot_loot_tag add foreign key (loot_id) references loot (id);
alter table loot_loot_tag add foreign key (loot_tag_id) references loot_tag (id);
alter table character_loot add foreign key (character_id) references character (id);
alter table character_loot add foreign key (loot_id) references loot (id);
alter table loot add foreign key (loot_table_id) references loot_table (id);
alter table loot add foreign key (loot_type_id) references loot_type (id);
alter table character_loot_equipped add foreign key (loot_id) references loot (id);
alter table character_loot_equipped add foreign key (character_id) references character (id);
alter table character add foreign key (class_id) references class (id);
alter table character add foreign key (bioform_id) references bioform (id);

