create table loot_type (
  id serial not null,
  name text not null,
  is_consumable boolean not null default false,
  should_pluralize boolean not null default false,
  primary key (id)
);
