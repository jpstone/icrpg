create table character_loot_equipped (
  id serial not null,
  loot_id int not null,
  character_id int not null,
  primary key (id)
);

create index equipped_loot_id on character_loot_equipped (loot_id);
create index equipped_character_id on character_loot_equipped (character_id);
