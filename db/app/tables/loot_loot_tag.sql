create table loot_loot_tag (
  id serial not null,
  loot_id int not null,
  loot_tag_id int not null,
  primary key (id)
);

create index loot_loot_tag_loot_id on loot_loot_tag (loot_id);
create index loot_loot_tag_loot_tag_id on loot_loot_tag (loot_tag_id);
