create table loot_tag (
  id serial not null,
  name text not null,
  description text not null,
  primary key (id)
);
