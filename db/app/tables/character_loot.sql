create table character_loot (
  id serial not null,
  character_id int not null,
  loot_id int not null,
  primary key (id)
);

create index character_loot_character_id on character_loot (character_id);
create index character_loot_loot_id on character_loot (loot_id);
