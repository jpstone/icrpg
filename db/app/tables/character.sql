create table character (
  id serial not null,
  name text null,
  str integer not null default 0,
  dex integer not null default 0,
  con integer not null default 0,
  int integer not null default 0,
  wis integer not null default 0,
  cha integer not null default 0,
  armor integer not null default 0,
  basic_effort integer not null default 0,
  weapon_effort integer not null default 0,
  magic_effort integer not null default 0,
  ultimate_effort integer not null default 0,
  attempt integer not null default 0,
  effort integer not null default 0,
  heart integer not null default 1,
  damage integer not null default 0,
  ammo integer not null default 0,
  coin integer not null default 0,
  class_id integer not null default 0,
  bioform_id integer not null default 0,
  note text null,
  primary key (id)
);

create index character_class_id on character (class_id);
create index character_bioform_id on character (bioform_id);
