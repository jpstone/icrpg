create table loot (
  id serial not null,
  name text not null,
  effect text null,
  description text null,
  loot_table_id int null,
  loot_type_id int null,
  primary key (id)
);

create index loot_loot_table_id on loot (loot_table_id);
create index loot_loot_type_id on loot (loot_type_id);
