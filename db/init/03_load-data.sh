#!/bin/bash

cd /app/data
for fullfile in ./*.csv; do
  filename=$(basename -- "$fullfile")
  tablename="${filename%.*}"
  firstline=$(head -n 1 "$fullfile")
  columns=$(echo "$firstline" | tr ';' ',')
  psql -c "copy ${tablename} (${columns}) from '/app/data/${filename}' delimiters ';' csv header" $POSTGRES_ICRPG_DB $POSTGRES_USER
done
