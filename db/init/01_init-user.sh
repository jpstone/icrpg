#!/bin/bash

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  create user $POSTGRES_ICRPG_USER PASSWORD '$POSTGRES_PASSWORD';
  create database $POSTGRES_ICRPG_DB;
  grant all privileges on database $POSTGRES_ICRPG_DB to $POSTGRES_ICRPG_DB;
EOSQL
