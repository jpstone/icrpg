#!/bin/bash

cd /app/tables
for filename in ./*.sql; do
  psql -bf "$filename" $POSTGRES_ICRPG_DB $POSTGRES_ICRPG_USER
done
