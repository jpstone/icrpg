const { gql } = require('apollo-server');

module.exports = gql`
type Query {
  characters: [Character]!
  character(id: Int): Character
  lootTables: [LootTable]!
  lootTypes: [LootType]!
}

type Character {
  id: Int!
  name: String!
  str: Int!
  dex: Int!
  con: Int!
  int: Int! wis: Int!
  cha: Int!
  armor: Int!
  basic_effort: Int!
  weapon_effort: Int!
  magic_effort: Int!
  ultimate_effort: Int!
  attempt: Int!
  effort: Int!
  heart: Int!
  damage: Int!
  ammo: Int!
  coin: Int!
  class: Class!
  bioform: Bioform!
  loot: [Loot],
  lootTypes: [LootType]!
}

input CreateCharacter {
  name: String!
  str: Int
  dex: Int
  con: Int
  int: Int
  wis: Int
  cha: Int
  armor: Int
  basic_effort: Int
  weapon_effort: Int
  magic_effort: Int
  ultimate_effort: Int
  attempt: Int
  effort: Int
  heart: Int
  damage: Int
  ammo: Int
  coin: Int
  class_id: Int!
  bioform_id: Int!
  loot: [Int],
}

input UpdateCharacter {
  id: Int!
  name: String
  str: Int
  dex: Int
  con: Int
  int: Int
  wis: Int
  cha: Int
  armor: Int
  basic_effort: Int
  weapon_effort: Int
  magic_effort: Int
  ultimate_effort: Int
  attempt: Int
  effort: Int
  heart: Int
  damage: Int
  ammo: Int
  coin: Int
  class_id: Int
  bioform_id: Int
  loot: [Int],
}

type Bioform {
  id: Int!
  name: String!
}

input BioformInput {
  name: String!
}

type Class {
  id: Int!
  name: String!
}

type Loot {
  id: Int!
  name: String!
  effect: String
  description: String
  tags: [Tag]
  lootTable: LootTable
  lootType: LootType
  equipped_id: Int
}

type Tag {
  id: Int!
  name: String!
  description: String!
}

type LootTable {
  id: Int!
  name: String!
  loot: [Loot]!
}

type LootType {
  id: Int!
  name: String!
  is_consumable: Boolean!
  should_pluralize: Boolean!
  loot(charId: Int): [Loot]
}

type Mutation {
  equip(lootId: Int!, charId: Int!): [LootType]!
  unequip(equippedId: Int!, charId: Int!): [LootType]!
  createCharacter(character: CreateCharacter): Character!
  updateCharacter(character: UpdateCharacter): Character!
}
`;
