# Dependencies

Be sure these are installed first:

- Docker
- Docker Compose
- Node/NPM

# Installation

**NOTE**: The `docker-compose.yml` has fallback environment variables for `$POSTGRES_ICRPG_USER`, `$POSTGRES_ICRPG_DB`, and `$POSTGRES_PASSWORD`. If you want to define your own, create a `.env` file in your root directory and put them there as key value pairs, separated by `=` (example: `POSTGRES_PASSWORD=mypass`).

1. Clone the repo
2. `npm install` in the root directory
3. `docker-compose up` to launch the database
4. `npm start` to launch the client app

# Loot tables

All of the items in the database are ordered just as the are in ICRPG. If you don't have the book in front of you and need loot tables, I logged them to the browser's console on app load for now. So just have your characters roll for loot, and then in your browser's console/dev tools, store the logged output to a global variable (in Chrome, right-click -> `Store as global variable`). Then just grab the loot by typing int he console `temp1[<loot table index>].loot[<number on d100> - 1]`. Obviously this is crude but it'll get the job done until I can spend more time on this.

# Contributing

Contributions welcome!
