const { REACT_APP_API_PORT, REACT_APP_API_HOST } = process.env;

export const API_PORT = REACT_APP_API_PORT;
export const API_HOST = REACT_APP_API_HOST;
export const API_BASE_URL = `http://${API_HOST}:${API_PORT}/`;
export const BASIC_STATS = ['STR', 'DEX', 'CON', 'INT', 'WIS', 'CHA'];
export const EFFORT_STATS = ['Basic Effort', 'Weapon Effort', 'Magic Effort', 'Ultimate Effort'];
export const OTHER_STATS = ['Armor'];
export const ALL_STATS = [...BASIC_STATS, ...EFFORT_STATS, ...OTHER_STATS];
export const COMBINED_STATS = { BASIC_STATS, EFFORT_STATS, OTHER_STATS };
export const BASE_ARMOR = 10;
