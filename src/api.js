import axios from "axios";
import { API_BASE_URL } from './constants.js';
import * as queries from './queries.js';

const api = axios.create({
  baseURL: API_BASE_URL
});

function createM(query) {
  return {
    "query": `mutation { ${queries.stringify(query)} }`
  };
}

function createQ(query) {
  return `?query={ ${queries.stringify(query)} }`;
}

export function equip(lootId, charId) {
  const body = queries.equip()({ equip: { lootId, charId }, loot: { charId } });
  return api.post('', createM(body)).then(res => res.data.data.equip);
}

export function unequip(equippedId, charId) {
  const body = queries.equip('unequip')({ unequip: { equippedId }, loot: { charId } });
  return api.post('', createM(body)).then(res => res.data.data.unequip);
}

export function getChars() {
  return api.get(createQ(queries.characters()())).then(res => res.data.data.characters);
}

export function getChar(id) {
  const args = {
    character: { id },
    loot: { charId: id },
  };
  return api
    .get(createQ(queries.character()(args)))
    .then(res => res.data.data.character);
}

export function getLootTables() {
  return api.get(createQ(queries.lootTableLoot('lootTables')())).then(res => res.data.data.lootTables);
}
