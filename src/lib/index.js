import { BASIC_STATS, BASE_ARMOR, EFFORT_STATS } from '../constants.js';
import * as snakeCase from 'lodash.snakecase';

export function updateStats(char) {
  const lootList = char.lootTypes.reduce((updated, lootType) => [...updated, ...lootType.loot.filter(l => l.equipped_id)], []);
  const updatedChar = lootList.reduce((updated, loot) => {
    if (loot.lootType.is_consumable) return updated;
    const effects = loot.effect ? loot.effect.split(',') : [];
    effects.forEach(effect => {
      if (!effect) return;
      const splitter = effect.includes('+') ? '+' : '-';
      const split = effect.split(splitter).slice(1)[0].split(' ');
      let prop = snakeCase(split[1]);
      if (split.length > 2) {
        prop = snakeCase(split.slice(1).join(' '));
      }
      updated[prop] = splitter === '+'
        ? updated[prop] + parseInt(split[0], 10)
        : updated[prop] - parseInt(split[0], 10);
    });
    return updated;
  }, char);
  BASIC_STATS.forEach(stat => {
    updatedChar[stat.toLowerCase()] = updatedChar[stat.toLowerCase()] + updatedChar.attempt;
  });
  EFFORT_STATS.forEach(s => {
    const stat = snakeCase(s);
    updatedChar[stat] = updatedChar[stat] + updatedChar.effort;
  });
  return updatedChar;
}

export function getArmorClass(char) {
  return char.armor + BASE_ARMOR;
}
