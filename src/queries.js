import { gql } from 'apollo-boost';

const characterFields = 'id name str dex con int wis cha armor basic_effort magic_effort weapon_effort ultimate_effort attempt effort heart damage ammo coin';

function getArgs(arr = []) {
  if (!arr.length) return '';
  return `(${arr.reduce((str, arg) => `${str ? str + ', ' : ''}${arg}: $${arg}`, '')})`;
}

export function stringify({ prop, args, fields, nested = [] }) {
  console.log(nested);
  const nestedQuery = nested.reduce((str, obj) => `${str} ${stringify(obj)}`, '');
  return `${prop}${getArgs(args)} {${fields ? ' ' + fields : ''}${nestedQuery} }`;
}

function createQuery(defaultProp, defaultFields, defaultNested = []) {
  const n = Array.isArray(defaultNested) ? defaultNested : [defaultNested];

  function modifyQuery(prop = defaultProp, fields = defaultFields, nested = defaultNested) {
    return (args = {}) => ({
      prop,
      fields,
      args: args[prop],
      nested: n.map(q => typeof q === 'function' ? q(args) : q),
    });
  }

  modifyQuery.prop = defaultProp;
  modifyQuery.fields = defaultFields;
  modifyQuery.nested = defaultNested;

  return modifyQuery;
}

export const tag = createQuery('tag', 'id name description');
export const loot = createQuery('loot', 'id name effect description');
export const lootType = createQuery('lootType', 'id name is_consumable should_pluralize');
export const lootTable = createQuery('lootTable', 'id name');
export const lootTags = createQuery(loot.prop, loot.fields, tag('tags'));
export const lootTagsLootType = createQuery(loot.prop, loot.fields, [tag('tags'), lootType()]);
export const lootTypeCharacterLoot = createQuery(lootType.prop, lootType.fields, lootTagsLootType(loot.prop, `${loot.fields} equipped_id`));
export const lootLootType = createQuery(loot.prop, loot.fields, lootType());
export const lootTableLoot = createQuery(lootTable.prop, lootTable.fields, loot());
export const _class = createQuery('class', 'id name');
export const bioform = createQuery('bioform', 'id name');
export const characters = gql`{ ${stringify(createQuery('characters', 'id name')()())} }`;
export const character =
  gql`
    query($id: Int!, $charId: Int!) {
      ${stringify(
        createQuery(
          'character',
          characterFields,
          [_class(), bioform(), lootTypeCharacterLoot('lootTypes')]
        )()({ character: ['id'], loot: ['charId']})
      )}
    }
  `;
export const equip =
  gql`
    mutation($lootId: Int!, $charId: Int!) {
      ${stringify(
        createQuery(
          'equip',
          lootTypeCharacterLoot.fields,
          lootTypeCharacterLoot.nested
        )()({ equip: ['lootId', 'charId'], loot: ['charId'] })
      )}
    }
  `;
export const unequip =
  gql`
    mutation($equippedId: Int!, $charId: Int!) {
      ${stringify(
        createQuery(
        'unequip',
        lootTypeCharacterLoot.fields,
        lootTypeCharacterLoot.nested)()({ unequip: ['equippedId', 'charId'], loot: ['charId'] })
      )}
    }
  `;
