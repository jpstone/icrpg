import React, { useState, useEffect, Fragment } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import * as snakeCase from 'lodash.snakecase';
import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import './App.css';
import * as api from './api.js';
import * as queries from './queries.js';
import { COMBINED_STATS, API_BASE_URL } from './constants.js';
import { updateStats, getArmorClass } from './lib';

const client = new ApolloClient({ uri: API_BASE_URL });

// api.getLootTables().then(console.log);

function Error({ error }) {
  return error
    ? error.response.status
    : null;
}

function CharList({ match }) {
  const { loading, error, data } = useQuery(queries.characters);

  if (error) return <Error error={error} />;
  if (loading) return 'Loading...';

  return (
    <div className="chars">
      <h1>Characters</h1>
      {data.characters.map(char => (
        <div className="chars-list" key={char.id}>
          <Link to={`character/${char.id}`}>
            {char.name}
          </Link>
        </div>
      ))}
    </div>
  );
}

function CharStatList({ stats, char, includeMod, bonus = 0 }) {
  return (
    <div className="stats">
      {
        stats.map(stat => {
          const val = char[snakeCase(stat)] + bonus;
          const op = val < 0 ? '-' : '+';
          return (
            <div className="stat" key={stat}>
              <span>{ stat.split(' ')[0] } {includeMod && op}{ Math.abs(val) }</span>
            </div>
          );
        })
      }
    </div>
  );
}

function LootItem({ loot, toggleEquipped, isEquipped }) {
  return (
    <Fragment>
      <h4>
        { isEquipped && <span className="loot-equipped" /> }
        <span className={`loot-name`}>{ loot.name } {loot.effect && `(${loot.effect})`}</span>
        <button type="button" className="anchor-button" onClick={() => toggleEquipped(loot, isEquipped)}>{ isEquipped ? 'unequip' : 'equip' }</button>
      </h4>
      <ul>
        { loot.description && <li>{ loot.description }</li> }
        { loot.tags && loot.tags.map(tag => <li key={tag.id}>{ tag.description }</li>) }
      </ul>
    </Fragment>
  );
}

function LootList({ loot, type, toggleEquipped }) {
  return Boolean(loot.length) && (
    <Fragment>
      <h1 className={`loot-list-header loot-list-header-${type.name.toLowerCase()}`}>
        { type.name }{ type.should_pluralize && 's'}
      </h1>
      { loot.map(l =>(
        <LootItem
          key={l.id}
          loot={l}
          toggleEquipped={toggleEquipped}
          isEquipped={Boolean(l.equipped_id)}
        />
      )) }
    </Fragment>
  );
}

function CharDetail({ match }) {
  const { params } = match;
  const id = parseInt(params.id, 10);
  const { error, loading, data } = useQuery(queries.character, { variables: { id, charId: id }});
  const [equip, equipRes] = useMutation(queries.equip);
  const [unequip, unequipRes] = useMutation(queries.unequip);

  console.log(equipRes);

  if (error) return <Error error={error} />;
  if (loading) return 'Loading...';

  const char = updateStats({ ...data.character });

  function toggleEquipped(loot, isEquipped) {
    isEquipped ? (
      unequip({ variables: { equippedId: loot.equipped_id, charId: id } })
    ) : (
      equip({ variables: { lootId: loot.id, charId: id }})
    );
  }

  return (
    <div className="char-item">
      <h1 className="char-name">
        <div className="ac">
          <span>{ getArmorClass(char) }</span>
        </div>
        <span>{ char.name }</span>
        { Array.from({ length: char.heart }, (x, y) => <div key={y} className="heart" />) }
      </h1>
      <CharStatList stats={COMBINED_STATS.BASIC_STATS} char={char} includeMod />
      <h2>Effort Bonuses</h2>
      <CharStatList stats={COMBINED_STATS.EFFORT_STATS} char={char} includeMod />
      { char.lootTypes.map(type => <LootList key={type.id} loot={type.loot} type={type} toggleEquipped={toggleEquipped} />)}
    </div>
  );
}

function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <div className="App">
          <Route path="/" exact component={CharList} />
          <Route path="/character" exact component={CharDetail} />
          <Route path="/character/:id" component={CharDetail} />
        </div>
      </Router>
    </ApolloProvider>
  );
}

export default App;
