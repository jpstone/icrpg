const { Client } = require('pg');
const { ApolloServer, gql } = require('apollo-server');
const { DataSource } = require('apollo-datasource');
const typeDefs = require('./type-defs.js');

const { POSTGRES_ICRPG_USER = 'icrpg', POSTGRES_PASSWORD = 'password', POSTGRES_ICRPG_DB = 'icrpg' } = process.env;
const db = new Client({ connectionString: `postgres://${POSTGRES_ICRPG_USER}:${POSTGRES_PASSWORD}@localhost/${POSTGRES_ICRPG_DB}` });
db.connect();

const insertCharLootQuery = 'insert into character_loot (loot_id, character_id) values ($1, $2)';

function query(q, v) {
  return v
    ? db.query(`${q};`, v).then(({ rows }) => rows)
    : db.query(`${q};`).then(({ rows }) => rows);
}

function singular(arr) {
  return arr[0];
}

function getCharacter(id) {
  return query(`select * from character where id = ${id}`).then(singular);
}

function getCharacterLoot(id) {
  return query(`
    select * from character_loot
    inner join loot on (character_loot.loot_id = loot.id)
    where character_loot.character_id = ${id}
  `);
}

function getLootTags(id) {
  return query(`
    select * from loot_loot_tag
    inner join loot_tag on (loot_loot_tag.loot_tag_id = loot_tag.id)
    where loot_loot_tag.loot_id = ${id}
  `);
}

function getLootTypeLoot(id) {
  return query(`
    select * from loot_type
    inner join loot on (loot_type.id = loot.loot_type_id)
    where loot_type.id = ${id}
  `);
}

function getLootTypes() {
  return query(`
    select * from loot_type
    order by case name
      when 'Weapon' then 1
      when 'Armor' then 2
      when 'Milestone' then 3
      when 'INT Spell' then 4
      when 'WIS Spell' then 5
      when 'Item' then 6
      when 'Ammo' then 7
      when 'Food' then 8
      else 9
    end
`);
}

function getCharacterLootTypeLoot(lootTypeId, { charId }) {
  return query(`
    select e.id as equipped_id, l.* from character_loot cl
    left outer join character_loot_equipped e on (e.loot_id = cl.loot_id and e.character_id = ${charId})
    inner join loot l on (l.id = cl.loot_id and l.loot_type_id = ${lootTypeId})
    where cl.character_id = ${charId}
  `);
}

function createCharacter(parent, args) {
  const { loot = [], ...rest } = args.character;
  const keys = Object.keys(rest);
  const values = keys.map(k => rest[k]);
  return query(`insert into character (${keys.join(',')}) values (${keys.map((k, i) => `$${i + 1}`).join(',')}) returning *`, values)
    .then(([char]) => {
      if (!loot.length) return char;
      const charLoot = loot.map(l => query(insertCharLootQuery), [l.id, char.id]);
      return Promise.all(charLoot).then(() => char);
    });
}

function updateCharacter(parent, args) {
  const { loot = [], id, ...rest } = args.character;
  const keys = Object.keys(rest);
  const values = keys.map(k => rest[k]);
  const deleteCharLoot = 'delete from character_loot where character_id = $1';
  return query(`update character set ${keys.map((k, i) => `${k}=$${i + 1}`)} where id = ${id} returning *`, values)
    .then(([char]) =>
      query(deleteCharLoot, [id])
      .then(() => Promise.all(loot.map(l => query(insertCharLootQuery, [l.id, char.id]))).then(() => char)));
}

const server = new ApolloServer({
  typeDefs,
  resolvers: {
    Query: {
      characters: () => query('select * from character'),
      character: (parent, args) => getCharacter(args.id),
      lootTables: () => query('select * from loot_table'),
      lootTypes: getLootTypes,
    },
    Character: {
      loot: parent => getCharacterLoot(parent.id),
      class: parent => query(`select * from class where id = ${parent.class_id}`).then(singular),
      bioform: parent => query(`select * from bioform where id = ${parent.bioform_id}`).then(singular),
      lootTypes: getLootTypes,
    },
    Loot: {
      lootType: parent => query(`select * from loot_type where id = ${parent.loot_type_id}`).then(singular),
      tags: parent => getLootTags(parent.id),
      lootTable: parent => query(`select * from loot_table where id = ${parent.loot_table_id}`),
    },
    LootTable: {
      loot: parent => query(`select * from loot where loot_table_id = ${parent.id}`),
    },
    LootType: {
      loot: (parent, args) => getCharacterLootTypeLoot(parent.id, args),
    },
    Mutation: {
      equip: (parent, { charId, lootId }) =>
        query(`insert into character_loot_equipped (character_id, loot_id) values (${charId}, ${lootId})`)
        .then(getLootTypes),
      unequip: (parent, { equippedId }) =>
        query(`delete from character_loot_equipped where id = ${equippedId}`)
        .then(getLootTypes),
      createCharacter,
      updateCharacter,
    },
  },
});

server.listen(process.env.API_PORT || 3001).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
